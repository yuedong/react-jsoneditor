import React from "react";

import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";
import JsonEditor from "../src/index";

class Editore extends React.Component {
  state = {
    obj: {
      hellu: "rei",
      oispa: "kaljaa"
    },
    temp: undefined
  };

  render() {
    return (
      <div>
        <button
          onClick={e => {
            this.setState({
              obj: {
                ...this.state.obj,
                ...this.state.temp,
                tussin: "lussutus"
              }
            });
          }}
        >
          change
        </button>
        <JsonEditor
          value={this.state.obj}
          onChange={temp => {
            this.setState({
              temp
            });
          }}
        />
      </div>
    );
  }
}

const json = {
  strinkula: "olen strinkula",
  number: 167.5,
  obu: {
    olen: "obukka",
    array: [1, "kaksi", 3.333]
  }
};

storiesOf("JsonEditor", module)
  .add("Default", () =>
    <JsonEditor value={json} onChange={action("changed")} onDirty={action("dirty")} />
  )
  .add("Small", () =>
    <JsonEditor
      width="500px"
      height="300px"
      value={json}
      onChange={action("changed")}
      onDirty={action("dirty")}
    />
  )
  .add("Auto width", () =>
    <JsonEditor
      width="auto"
      height="300px"
      value={json}
      onChange={action("changed")}
      onDirty={action("dirty")}
    />
  )
  .add("External change", () => <Editore />)
  .add("Disabling some options", () =>
    <JsonEditor
      width="auto"
      height="300px"
      value={json}
      onChange={action("changed")}
      onDirty={action("dirty")}
      options={{
        search: false,
        history: false
      }}
    />
  )
  .add("Code mode", () =>
    <JsonEditor
      value={json}
      onChange={action("changed")}
      onDirty={action("dirty")}
      options={{
        mode: "code"
      }}
    />
  );
