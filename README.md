# Json editor component for React

## What?

This component is a React wrapper for the [jsoneditor](https://github.com/josdejong/jsoneditor) NPM package.

## Why?

Did not find a reasonable JSON editor component for react. Needed one.
Did some research. Found a good editor library. Wrapped it.

## How?

```javascript

import ReactDOM from 'react-dom';
import JsonEditor from '@dr-kobros/react-jsoneditor';

const json = {
  tussin: 'lussutus',
  number: 666,
  array: [
    1,
    2,
    3,
  ],
  obu: {
    lipaiseppa: 'ankkaa',
  }
};

const onChange = json => {
  console.log(json);
};

// Editor configuration. See jsoneditor's API.
const options = {

}

ReactDOM.render(
  <JsonEditor value={json} onChange={onChange} options={options} />,
  document.getElementById('some_element')
);

```

## examples

* fork repo
* `yarn`
* `yarn run storybook`
